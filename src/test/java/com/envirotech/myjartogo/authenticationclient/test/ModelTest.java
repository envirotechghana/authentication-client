package com.envirotech.myjartogo.authenticationclient.test;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import javax.security.auth.login.AppConfigurationEntry.LoginModuleControlFlag;

import static org.hamcrest.core.Is.*;

import org.junit.Test;

import com.envirotech.myjartogo.system.authentication.model.TokenResponse;
import com.envirotech.myjartogo.system.authentication.model.LoginConfigurationEntry.ModuleFlag;
import com.envirotech.myjartogo.system.authentication.model.Registration;
import com.quakearts.webapp.security.jwt.JWTClaims;
import com.quakearts.webapp.security.jwt.JWTHeader;
import com.quakearts.webapp.security.jwt.JWTSigner;
import com.quakearts.webapp.security.jwt.factory.JWTFactory;

public class ModelTest {

	@Test
	public void testTokenHasExpired() throws Exception {
		JWTFactory factory = JWTFactory.getInstance();
		JWTHeader header = factory.createEmptyClaimsHeader();
		JWTClaims claims = factory.createEmptyClaims();
		claims.setAudience("http://test.com");
		claims.setIssuedAt((System.currentTimeMillis()-67000)/1000);
		claims.setIssuer("http://test.com");
		claims.setExpiry((System.currentTimeMillis()-6000)/1000);
		claims.setNotBefore((System.currentTimeMillis()-66000)/1000);
		claims.setSubject("test");
		
		Map<String, String> options = new HashMap<>();
		options.put("secret", "SECRET");
		JWTSigner jwtSigner = factory.getSigner("HS256", options);
		String token = jwtSigner.sign(header, claims);
		
		TokenResponse response = new TokenResponse();
		response.setExpiresIn(60);
		response.setTokenType("bearer");
		response.setIdToken(token);
		
		assertThat(response.hasExpired(), is(true));
		
		claims = factory.createEmptyClaims();
		claims.setAudience("http://test.com");
		claims.setIssuedAt((System.currentTimeMillis()-7000)/1000);
		claims.setIssuer("http://test.com");
		claims.setExpiry((System.currentTimeMillis()+60000)/1000);
		claims.setNotBefore((System.currentTimeMillis())/1000);
		claims.setSubject("test");
		
		token = jwtSigner.sign(header, claims);
		response.setIdToken(token);
		assertThat(response.hasExpired(), is(false));
		
		claims = factory.createEmptyClaims();
		claims.setAudience("http://test.com");
		claims.setIssuedAt((System.currentTimeMillis()-67000)/1000);
		claims.setIssuer("http://test.com");
		claims.setExpiry((System.currentTimeMillis()-1000)/1000);
		claims.setNotBefore((System.currentTimeMillis()-66000)/1000);
		claims.setSubject("test");
		
		token = jwtSigner.sign(header, claims);
		response.setIdToken(token);
		assertThat(response.hasExpired(), is(true));
	}
	
	@Test
	public void testModuleFlags() throws Exception {
		assertThat(ModuleFlag.OPTIONAL.getFlag(), is(LoginModuleControlFlag.OPTIONAL));
		assertThat(ModuleFlag.REQUIRED.getFlag(), is(LoginModuleControlFlag.REQUIRED));
		assertThat(ModuleFlag.REQUISITE.getFlag(), is(LoginModuleControlFlag.REQUISITE));
		assertThat(ModuleFlag.SUFFICIENT.getFlag(), is(LoginModuleControlFlag.SUFFICIENT));
	}

	@Test
	public void testRegistrationBuilderTest() throws Exception {
		Registration registration = new Registration();
		registration.setAlias("alias");
		registration.setId("id");
		registration.createConfiguration()
			.setNameAs("name")
			.createEntry()
				.setModuleClassnameAs("classname")
				.setModuleFlagAs(ModuleFlag.OPTIONAL)
				.addOption("test", "option")
				.addOption("test2", "option2")
				.thenAdd()
			.thenAdd()
			.addOption("test-main", "main option");
		
		assertThat(registration.getAlias(), is("alias"));
		assertThat(registration.getId(), is("id"));
		assertThat(registration.getConfigurations().size(), is(1));
		assertThat(registration.getConfigurations().get(0).getName(), is("name"));
		assertThat(registration.getConfigurations().get(0).getEntries().size(), is(1));
		assertThat(registration.getConfigurations().get(0).getEntries().get(0).getModuleClassname(), is("classname"));
		assertThat(registration.getConfigurations().get(0).getEntries().get(0).getModuleFlag(), is(ModuleFlag.OPTIONAL));
		assertThat(registration.getConfigurations().get(0).getEntries().get(0).getOptions().size(), is(2));
		assertThat(registration.getConfigurations().get(0).getEntries().get(0).getOptions().get("test"), is("option"));
		assertThat(registration.getConfigurations().get(0).getEntries().get(0).getOptions().get("test2"), is("option2"));
		assertThat(registration.getOptions().size(), is(1));
		assertThat(registration.getOptions().get("test-main"), is("main option"));
	}
}
