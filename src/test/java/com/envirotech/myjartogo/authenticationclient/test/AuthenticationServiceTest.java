package com.envirotech.myjartogo.authenticationclient.test;

import static com.quakearts.tools.test.mockserver.model.impl.HttpMessageBuilder.createNewHttpRequest;
import static com.quakearts.tools.test.mockserver.model.impl.HttpMessageBuilder.createNewHttpResponse;
import static com.quakearts.tools.test.mockserver.model.impl.MockActionBuilder.createNewMockAction;
import static org.junit.Assert.*;
import static org.hamcrest.core.Is.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.security.auth.Subject;
import javax.security.auth.login.LoginException;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import com.envirotech.myjartogo.system.authentication.AuthenticationServerError;
import com.envirotech.myjartogo.system.authentication.AuthenticationService;
import com.envirotech.myjartogo.system.authentication.model.ErrorResponse;
import com.envirotech.myjartogo.system.authentication.model.LoginConfiguration;
import com.envirotech.myjartogo.system.authentication.model.LoginConfigurationEntry;
import com.envirotech.myjartogo.system.authentication.model.Registration;
import com.envirotech.myjartogo.system.authentication.model.RegistrationResponse;
import com.envirotech.myjartogo.system.authentication.model.TokenResponse;
import com.envirotech.myjartogo.system.authentication.model.LoginConfigurationEntry.ModuleFlag;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quakearts.rest.client.exception.HttpClientException;
import com.quakearts.tools.test.mockserver.MockServer;
import com.quakearts.tools.test.mockserver.MockServerFactory;
import com.quakearts.tools.test.mockserver.configuration.Configuration.MockingMode;
import com.quakearts.tools.test.mockserver.configuration.impl.ConfigurationBuilder;
import com.quakearts.tools.test.mockserver.fi.HttpRequestMatcher;
import com.quakearts.tools.test.mockserver.store.HttpMessageStore;
import com.quakearts.tools.test.mockserver.store.impl.MockServletHttpMessageStore;
import com.quakearts.webapp.security.auth.JWTLoginModule;
import com.quakearts.webapp.security.auth.JWTPrincipal;
import com.quakearts.webapp.security.auth.UserPrincipal;
import com.quakearts.webtools.test.CDIRunner;

import junit.framework.AssertionFailedError;

@RunWith(CDIRunner.class)
public class AuthenticationServiceTest {
	private static MockServer mockServer;

	private static JWTPrincipal jwtPrincipal;

	@Inject
	private AuthenticationService service;
	
	@BeforeClass
	public static void startMockServer() throws Exception {
		HttpMessageStore store = MockServletHttpMessageStore
				.getInstance();
							
		mockServer = MockServerFactory
				.getInstance().getMockServer()
				.configure(ConfigurationBuilder
						.newConfiguration()
						.setMockingModeAs(MockingMode.MOCK)
						.setPortAs(8081)
						.thenBuild())
				.add(createNewMockAction()
							.setRequestAs(store.findRequestIdentifiedBy("GET-Registration-Initial"))
							.thenBuild(),
						createNewMockAction()
							.setRequestAs(store.findRequestIdentifiedBy("POST-Registration"))
							.setMatcherAs(createMatcher())
							.thenBuild(),
						createNewMockAction()
							.setRequestAs(createNewHttpRequest()
							.setId("GET-Authenticate-User")
							.setResourceAs("/authenticate/system-app/My-Jartogo-System?clientId=testuser&credential=password1")
							.setMethodAs("GET")
							.setResponseAs(createNewHttpResponse()
									.setResponseCodeAs(200)
									.setContentBytes(createToken())
									.thenBuild())
							.thenBuild())
						.thenBuild(),
						createNewMockAction()
							.setRequestAs(createNewHttpRequest()
							.setId("GET-Authenticate-User-Wrong-Password")
							.setResourceAs("/authenticate/system-app/My-Jartogo-System?clientId=testuser&credential=passwordWrong")
							.setMethodAs("GET")
							.setResponseAs(createNewHttpResponse()
									.setResponseCodeAs(403)
									.setContentBytes(createErrorResponse())
									.thenBuild())
							.thenBuild())
						.thenBuild(),
						createNewMockAction()
							.setRequestAs(createNewHttpRequest()
							.setId("GET-Authenticate-User-Uknown-Error")
							.setResourceAs("/authenticate/system-app/Unknown-Error?clientId=testuser&credential=password1")
							.setMethodAs("GET")
							.setResponseAs(createNewHttpResponse()
									.setResponseCodeAs(403)
									.setContentBytes("<unknown/><error/>".getBytes())
									.thenBuild())
							.thenBuild())
						.thenBuild(),
						createNewMockAction()
							.setRequestAs(createNewHttpRequest()
							.setId("GET-Authenticate-User-Uknown-Error")
							.setResourceAs("/authenticate/system-app/Wrong-Response?clientId=testuser&credential=password1")
							.setMethodAs("GET")
							.setResponseAs(createNewHttpResponse()
									.setResponseCodeAs(200)
									.setContentBytes("<wrong/><response/>".getBytes())
									.thenBuild())
							.thenBuild())
						.thenBuild());
		mockServer.start();
	}
	
	private static HttpRequestMatcher createMatcher() {
		return (request, incomingRequest)->{
			boolean match = request.getResource().equals(incomingRequest.getResource())
					&& request.getMethod().equals(incomingRequest.getMethod());
			
			if(match){
				ObjectMapper mapper = CDI.current().select(ObjectMapper.class).get();
				try {
					Registration incoming = mapper.readValue(incomingRequest.getContentBytes(), Registration.class);
					Registration compare = mapper.readValue(incomingRequest.getContentBytes(), Registration.class);
					
					match = incoming.getAlias().equals(compare.getAlias())
							&& incoming.getId().equals(compare.getId())
							&& incoming.getConfigurations().size() 
								== compare.getConfigurations().size()
							&& incoming.getConfigurations().size() 
							== 1
							&& incoming.getOptions().equals(compare.getOptions());
					
					LoginConfiguration incomingLoginConfiguration = incoming.getConfigurations().get(0);
					LoginConfiguration compareLoginConfiguration = compare.getConfigurations().get(0);
					if(match){
						match = incomingLoginConfiguration.getName()
								.equals(compareLoginConfiguration.getName())
								&& incomingLoginConfiguration.getEntries().size() 
								== compareLoginConfiguration.getEntries().size()
								&& incomingLoginConfiguration.getEntries().size() == 1;
					}
					
					LoginConfigurationEntry incomingConfigurationEntry =
							incomingLoginConfiguration.getEntries().get(0);
					LoginConfigurationEntry compareConfigurationEntry =
							incomingLoginConfiguration.getEntries().get(0);
					if(match){
						match = incomingConfigurationEntry
								.getModuleClassname().equals(compareConfigurationEntry.getModuleClassname())
								&& incomingConfigurationEntry.getModuleFlag() ==
								compareConfigurationEntry.getModuleFlag()
								&& incomingConfigurationEntry.getOptions().isEmpty()
								&& compareConfigurationEntry.getOptions().isEmpty();
					}
					
					match = compare.getOptions().equals(incoming.getOptions());
				} catch (IOException e) {
					throw new AssertionFailedError(e.getMessage());
				}
				
			}
			
			return match;
		};
	}

	private static byte[] createToken() {
		Subject subject = new Subject();
		HashMap<String, String> options = new HashMap<>();
		options.put("algorithm", "HS256");
		options.put("issuer", "https://authenticator.envirotech.com");
		options.put("audience", "https://myjartogo.envirotech.com");
		options.put("secret", "SECRET");
		options.put("validity", "5 Minutes");
		options.put("additional.claims", "Read;Write;");
		HashMap<String, Object> state = new HashMap<>();
		state.put("javax.security.auth.login.name", new UserPrincipal("testuser"));
		state.put("com.quakearts.LoginOk", Boolean.TRUE);
		
		JWTLoginModule jwtLoginModule = new JWTLoginModule();
		jwtLoginModule.initialize(subject, callbacks->{}, state, options);
		try {
			jwtLoginModule.login();
			jwtLoginModule.commit();
		} catch (LoginException e) {
			throw new AssertionFailedError(e.getMessage());
		}
		
		jwtPrincipal = subject.getPrincipals(JWTPrincipal.class)
				.iterator().next();
		
		
		return ("{\r\n" + 
				"    \"tokenType\": \"bearer\",\r\n" + 
				"    \"expiresIn\": 300,\r\n" + 
				"    \"idToken\": \""+jwtPrincipal.getName()
				+"\"\r\n" + 
				"}").getBytes();
	}
	
	private static byte[] createErrorResponse() {
		return ("{\r\n" + 
				"	\"code\": \"test-error\",\r\n" + 
				"	\"explanations\": [\r\n" + 
				"		\"Test Explanation 1\",\r\n" + 
				"		\"Test Explanation 2\"\r\n" + 
				"	]\r\n" + 
				"}").getBytes();
	}

	@Test
	public void testService() throws Exception {
		List<String> aliases = service.getRegisteredAliases();
		assertThat(aliases.size(), is(1));
		
		Registration registration = new Registration();
		
		registration
			.withAliasAs("system-app")
			.withIdAs("01fa316f4ec2297c9e26de85a7992a56d2618387893d8308a61758fbf84d11a5")
				.createConfiguration()
			.setNameAs("My-Jartogo-System")
			.createEntry()
				.setModuleClassnameAs("com.envirotech.security.PropertyFileLoginModule")
				.setModuleFlagAs(ModuleFlag.REQUIRED)
				.thenAdd()
			.thenAdd()
			.addOption("audience", "https://myjartogo.envirotech.com")
			.addOption("grace.period", "30")
			.addOption("validity.period", "1 Day")
			.addOption("secret", "SECRET")
			.addOption("issuer", "https://authenticator.envirotech.com")
			.addOption("algorithm", "HS256");
		RegistrationResponse response = service.registerAuthentication(registration);
		
		assertThat(response.getHttpCode(), is(204));
		
		TokenResponse tokenResponse = service.authenticate("system-app","My-Jartogo-System","testuser","password1");
		assertThat(tokenResponse.getExpiresIn(), is(300L));
		assertThat(tokenResponse.getTokenType(), is("bearer"));
		assertThat(tokenResponse.getIdToken(), is(jwtPrincipal.getName()));
	}

	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void testResponseError() throws Exception {
		expectedException.expect(AuthenticationServerError.class);
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setCode("test-error");
		errorResponse.setExplanations(Arrays.asList("Test Explanation 1","Test Explanation 2"));
		AuthenticationServerError error = new AuthenticationServerError(errorResponse, 403);
		expectedException.expect(isException(error));
		service.authenticate("system-app","My-Jartogo-System","testuser","passwordWrong");
	}
	
	@Test
	public void testRequestMarshallingError() throws Exception {
		expectedException.expect(HttpClientException.class);
		expectedException.expectMessage("Unable to marshal request object");
		service.registerAuthentication(new RegistrationBomb());
	}
	
	@Test
	public void testResponseMarshallingError() throws Exception {
		expectedException.expect(AuthenticationServerError.class);
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setCode("uknown-error");
		errorResponse.setExplanations(Arrays.asList("<wrong/><response/>"));
		AuthenticationServerError error = new AuthenticationServerError(errorResponse, 200);
		expectedException.expect(isException(error));
		service.authenticate("system-app","Wrong-Response","testuser","password1");
	}
	
	@Test
	public void testResponseErrorMarshallingError() throws Exception {
		expectedException.expect(AuthenticationServerError.class);
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setCode("uknown-error");
		errorResponse.setExplanations(Arrays.asList("<unknown/><error/>"));
		AuthenticationServerError error = new AuthenticationServerError(errorResponse, 403);
		expectedException.expect(isException(error));
		service.authenticate("system-app","Unknown-Error","testuser","password1");
	}
	
	private Matcher<Throwable> isException(AuthenticationServerError error){
		return new BaseMatcher<Throwable>() {
			@Override
			public void describeTo(Description description) {
				description.appendText("AuthenticationServer error with message ")
				.appendText(error.getMessage()).appendText(" and error response code ")
				.appendText(error.getErrorResponse()!=null?error.getErrorResponse().getCode():"")
				.appendText(" and explanations ")
				.appendText(error.getErrorResponse()!=null
				&& error.getErrorResponse().getExplanations()!=null
				?error.getErrorResponse().getExplanations().toString():"");
			}
			
			@Override
			public boolean matches(Object item) {
				if(item instanceof AuthenticationServerError){
					AuthenticationServerError inError = (AuthenticationServerError) item;
					
					if(error.getHttpCode() != inError.getHttpCode())
						return false;
					
					if(error.getErrorResponse() == null)
						return error.getErrorResponse() == inError.getErrorResponse();
					
					boolean match = true;
					if(error.getErrorResponse().getCode() == null)
						match = error.getErrorResponse().getCode() == inError.getErrorResponse().getCode();
					else
						match = error.getErrorResponse().getCode().equals(inError.getErrorResponse().getCode());
					
					if(match && error.getErrorResponse().getExplanations() == null)
						return error.getErrorResponse().getExplanations() == inError.getErrorResponse().getExplanations();
					
					return match && error.getErrorResponse().getExplanations().equals(inError.getErrorResponse().getExplanations());
				}
				return false;
			}
		};
	}
	
	class RegistrationBomb extends Registration {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5986323006289590686L;
		
		@Override
		public String getAlias() {
			throw new UnsupportedOperationException();
		}
	}
}
