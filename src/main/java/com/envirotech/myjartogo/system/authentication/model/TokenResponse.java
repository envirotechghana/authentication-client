package com.envirotech.myjartogo.system.authentication.model;

import com.quakearts.webapp.security.jwt.JWTClaims;
import com.quakearts.webapp.security.jwt.factory.JWTFactory;

public class TokenResponse {
	private String tokenType;
	private long expiresIn;
	private String idToken;

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(long expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getIdToken() {
		return idToken;
	}

	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}

	public boolean hasExpired() {
		String[] parts = idToken.split("\\.", 3);
		JWTClaims claims = JWTFactory.getInstance()
				.createJWTClaimsFromBytes(parts[1].getBytes());
		return System.currentTimeMillis()-(claims.getExpiry()*1000) - 1000>0;
	}

}
