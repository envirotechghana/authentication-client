package com.envirotech.myjartogo.system.authentication;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.envirotech.myjartogo.cdi.annotations.ProducerTarget;
import com.envirotech.myjartogo.system.authentication.model.ErrorResponse;
import com.envirotech.myjartogo.system.authentication.model.Registration;
import com.envirotech.myjartogo.system.authentication.model.RegistrationResponse;
import com.envirotech.myjartogo.system.authentication.model.TokenResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quakearts.rest.client.HttpObjectClient;
import com.quakearts.rest.client.HttpResponse;
import com.quakearts.rest.client.HttpVerb;
import com.quakearts.rest.client.exception.HttpClientException;

@ProducerTarget
@Singleton
public class AuthenticationHttpClient 
	extends HttpObjectClient implements AuthenticationService {

	@Inject
	private ObjectMapper objectMapper;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7505325242909109456L;
	private static final String REGISTRATION_RESOURCE = "/registration";

	@Override
	@SuppressWarnings("unchecked")
	public List<String> getRegisteredAliases() throws HttpClientException, IOException {
		return executeGet(REGISTRATION_RESOURCE, List.class);
	}

	@Override
	public RegistrationResponse registerAuthentication(Registration registration)
			throws HttpClientException, IOException {
		return execute(HttpVerb.POST, REGISTRATION_RESOURCE, registration, "application/json",
				RegistrationResponse.class);
	}

	@Override
	public TokenResponse authenticate(String alias, String application, String cliendId, String credential) 
			throws IOException, HttpClientException {
		return executeGet("/authenticate/{0}/{1}?clientId={2}&credential={3}", 
				TokenResponse.class, alias, application, cliendId, credential);
	}
	
	@Override
	protected String writeValueAsString(Object requestValue) throws HttpClientException {
		try {
			return objectMapper.writeValueAsString(requestValue);
		} catch (JsonProcessingException e) {
			throw new HttpClientException("Unable to marshal request object", e);
		}
	}

	@Override
	protected HttpClientException nonSuccessResponseUsing(HttpResponse httpResponse) {
		try {
			return new AuthenticationServerError(objectMapper.readValue(httpResponse.getOutputBytes(),
					ErrorResponse.class),
					httpResponse.getHttpCode());
		} catch (IOException e) {
			return generateAuthenticationServerError(httpResponse);
		}
	}

	protected HttpClientException generateAuthenticationServerError(HttpResponse httpResponse) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setCode("uknown-error");
		errorResponse.setExplanations(Arrays.asList(httpResponse.getOutput()));
		return new AuthenticationServerError(errorResponse, httpResponse.getHttpCode());
	}

	@Override
	protected <R> Converter<R> createConverter(Class<R> targetClass) {
		return httpResponse -> {
			if(targetClass == RegistrationResponse.class) {
				return targetClass.cast(new RegistrationResponse()
						.withHttpCodeAs(httpResponse.getHttpCode()));
			}
			
			try {
				return objectMapper.readValue(httpResponse.getOutputBytes(), targetClass);
			} catch (IOException e) {
				throw generateAuthenticationServerError(httpResponse);
			}
		};
	}
}
