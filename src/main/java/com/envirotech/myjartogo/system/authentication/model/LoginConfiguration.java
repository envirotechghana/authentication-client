package com.envirotech.myjartogo.system.authentication.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LoginConfiguration implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8418439449230724666L;
	private String name;
	private List<LoginConfigurationEntry> entries = new ArrayList<>();
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<LoginConfigurationEntry> getEntries() {
		return entries;
	}
	
	public void setEntries(List<LoginConfigurationEntry> entries) {
		this.entries = entries;
	}
}