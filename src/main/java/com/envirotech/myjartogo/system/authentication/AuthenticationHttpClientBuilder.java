package com.envirotech.myjartogo.system.authentication;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.inject.Singleton;

import com.envirotech.myjartogo.cdi.annotations.ProducerTargetQualifier;
import com.envirotech.myjartogo.config.Environment;
import com.quakearts.rest.client.HttpClientBuilder;

@Singleton
public class AuthenticationHttpClientBuilder extends HttpClientBuilder<AuthenticationHttpClient> {

	@Inject
	private Environment environment;
	
	public AuthenticationHttpClientBuilder() {
		httpClient = CDI.current().select(AuthenticationHttpClient.class,
				new ProducerTargetQualifier()).get();
	}
	
	@Override
	public AuthenticationHttpClient thenBuild() {
		return httpClient;
	}
	
	@Produces @Default @Any
	public AuthenticationService createAuthenticationService() {
		return setURLAs(environment.get("authentication.url","http://localhost:8081"))
				.thenBuild();
	}
}
