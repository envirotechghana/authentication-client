package com.envirotech.myjartogo.system.authentication;

import java.io.IOException;
import java.util.List;

import com.envirotech.myjartogo.system.authentication.model.Registration;
import com.envirotech.myjartogo.system.authentication.model.RegistrationResponse;
import com.envirotech.myjartogo.system.authentication.model.TokenResponse;
import com.quakearts.rest.client.exception.HttpClientException;

public interface AuthenticationService {

	List<String> getRegisteredAliases() throws HttpClientException, IOException;

	RegistrationResponse registerAuthentication(Registration registration) throws HttpClientException, IOException;

	TokenResponse authenticate(String alias, String application, String cliendId, String credential) throws IOException, HttpClientException;

}