package com.envirotech.myjartogo.system.authentication;

import com.envirotech.myjartogo.system.authentication.model.ErrorResponse;
import com.quakearts.rest.client.exception.HttpClientException;

public class AuthenticationServerError extends HttpClientException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7373201103272643270L;
	private final int httpCode;
	private final ErrorResponse errorResponse;
	public AuthenticationServerError(ErrorResponse errorResponse, int httpCode) {
		super("Server Error");
		this.httpCode = httpCode;
		this.errorResponse = errorResponse;
	}
	
	public ErrorResponse getErrorResponse() {
		return errorResponse;
	}
	
	public int getHttpCode() {
		return httpCode;
	}
}
