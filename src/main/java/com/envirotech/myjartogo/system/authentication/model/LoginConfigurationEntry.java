package com.envirotech.myjartogo.system.authentication.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.login.AppConfigurationEntry.LoginModuleControlFlag;

public class LoginConfigurationEntry implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9187062519254714504L;

	private String moduleClassname;
	private ModuleFlag moduleFlag;
	public enum ModuleFlag {
		REQUIRED(LoginModuleControlFlag.REQUIRED), 
		REQUISITE(LoginModuleControlFlag.REQUISITE), 
		SUFFICIENT(LoginModuleControlFlag.SUFFICIENT), 
		OPTIONAL(LoginModuleControlFlag.OPTIONAL);

		private LoginModuleControlFlag flag;

		public LoginModuleControlFlag getFlag() {
			return flag;
		}

		private ModuleFlag(LoginModuleControlFlag flag) {
			this.flag = flag;
		}
	}

	private Map<String, String> options;
	
	public String getModuleClassname() {
		return moduleClassname;
	}

	public void setModuleClassname(String moduleClassname) {
		this.moduleClassname = moduleClassname;
	}

	public ModuleFlag getModuleFlag() {
		return moduleFlag;
	}

	public void setModuleFlag(ModuleFlag moduleFlag) {
		this.moduleFlag = moduleFlag;
	}

	public Map<String, String> getOptions() {
		if(options == null)
			setOptions(new HashMap<>());

		return options;
	}

	public void setOptions(Map<String, String> options) {
		this.options = options;
	}

}