package com.envirotech.myjartogo.system.authentication.model;

import com.envirotech.myjartogo.httpclient.HttpCodedResponse;

public class RegistrationResponse implements HttpCodedResponse {

	private int httpCode;
	
	@Override
	public void setHttpCode(int httpCode) {
		this.httpCode = httpCode;
	}

	public RegistrationResponse withHttpCodeAs(int httpCode) {
		setHttpCode(httpCode);
		return this;
	}
	
	@Override
	public int getHttpCode() {
		return httpCode;
	}

}
