package com.envirotech.myjartogo.system.authentication.model;

import java.io.Serializable;
import java.util.List;

public class ErrorResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6877221669417014199L;
	private String code;
	private List<String> explanations;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<String> getExplanations() {
		return explanations;
	}

	public void setExplanations(List<String> explanations) {
		this.explanations = explanations;
	}

}
